package kalashnikov.dmitry.lab3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.*;

/**
 * Created by kalasd on 4/29/2014.
 */
public class Dispatcher
{
    private String fileName;
    private ExecutorService executorService;
    private ExecutorCompletionService ecs;

    public Dispatcher(String fileName, int threadNum)
    {
        this.fileName = fileName;
        executorService = Executors.newFixedThreadPool(threadNum);
        ecs = new ExecutorCompletionService(executorService);
    }

    public int[] sort() throws IOException, InterruptedException, ExecutionException {
        LinkedList<Callable<int[]>> tasks = new LinkedList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName)))
        {
            String line = br.readLine();
            while (line != null) {
                StringTokenizer st = new StringTokenizer(line);
                String[] splitted = new String[st.countTokens()];
                int ind = 0;
                while (st.hasMoreElements()) {
                    splitted[ind] = st.nextElement().toString();
                    ++ind;
                }
                tasks.add(new SortTask(splitted));
                line = br.readLine();
            }
        }
        if (tasks.size() % 2 == 1) {
            tasks.add(new SortTask(new String[0]));
        }
        List<Future<int[]>> result = executorService.invokeAll(tasks);
        while (result.size() != 1) {
            tasks.clear();
            int i = 0;
            for (; i < result.size() - 2; i += 2) {
                tasks.add(new MergeTask(result.get(i).get(), result.get(i + 1).get()));
            }
            if (result.size() % 2 == 1) {
                tasks.add(new MergeTask(result.get(i).get(), new int[]{}));
            } else {
                tasks.add(new MergeTask(result.get(i).get(), result.get(i + 1).get()));
            }

            result = executorService.invokeAll(tasks);
        }
        return result.get(0).get();
    }
}
