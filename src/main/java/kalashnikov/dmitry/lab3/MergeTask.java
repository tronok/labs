package kalashnikov.dmitry.lab3;

import java.util.concurrent.Callable;

/**
 * Created by Tronok on 29.04.14.
 */
public class MergeTask implements Callable<int[]> {
    private int[] first;
    private int[] second;

    public MergeTask(int[] first, int[] second) {
        if (first == null || second == null) {
            throw new NullPointerException("Arrays must be not null");
        }
        this.first = first;
        this.second = second;
    }

    @Override
    public int[] call() throws Exception {
        int[] merged = new int[first.length + second.length];
        int i = 0, j = 0, k = 0;

        while (i != first.length && j != second.length) {
            if (first[i] < second[j]) {
                merged[k] = first[i];
                ++k;
                ++i;
            } else {
                merged[k] = second[j];
                ++k;
                ++j;
            }
        }
        while (i != first.length) {
            merged[k] = first[i];
            ++k;
            ++i;
        }
        while (j != second.length) {
            merged[k] = second[j];
            ++k;
            ++j;
        }
        return merged;
    }
}
