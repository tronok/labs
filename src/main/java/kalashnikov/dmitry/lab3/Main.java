package kalashnikov.dmitry.lab3;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

/**
 * Created by Tronok on 10.04.14.
 */
public class Main {
    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        if (args.length != 2)
        {
            throw new IllegalArgumentException("Length of args must be equal 2");
        }
        Dispatcher dispatcher = new Dispatcher(args[0], Integer.parseInt(args[1]));
        int[] result = dispatcher.sort();
        for (int i : result) {
            System.out.print(Integer.toString(i) + " ");
        }
    }


}
