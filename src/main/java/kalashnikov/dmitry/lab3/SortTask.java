package kalashnikov.dmitry.lab3;

import java.util.Arrays;
import java.util.concurrent.Callable;

/**
 * Created by Tronok on 29.04.14.
 */
public class SortTask implements Callable<int[]> {

    private int[] toSort;

    public SortTask(String[] toParse) {
        toSort = new int[toParse.length];
        for (int i = 0; i != toParse.length; ++i) {
            toSort[i] = Integer.parseInt(toParse[i]);
        }
    }

    @Override
    public int[] call() throws Exception {
        Arrays.sort(toSort);
        return toSort;
    }
}
