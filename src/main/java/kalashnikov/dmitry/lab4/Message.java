package kalashnikov.dmitry.lab4;

/**
 * Created by kalasd on 5/29/2014.
 */
public class Message {
    private String message;
    private String name;
    private String uuid;

    public Message(String message, String name, String uuid) {
        this.message = message;
        this.name = name;
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Message() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "(" + uuid + ") " + name + ": " + message;
    }
}
