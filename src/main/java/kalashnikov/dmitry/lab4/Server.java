package kalashnikov.dmitry.lab4;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by kalasd on 5/29/2014.
 */
public class Server {
    private final static int PORT = 9998;
    private static ExecutorService executorService = Executors.newCachedThreadPool();
    private static final Map<String, PrintWriter> users = new HashMap<>();

    public static void main(String[] args) throws IOException {

        ServerSocket serverSocket = new ServerSocket(PORT);
        while (true) {
            try {
                Socket socket = serverSocket.accept();
                executorService.execute(new Handler(socket));
            } catch (IOException ex) {
                ex.printStackTrace();
            }


        }

    }

    private static class Handler implements Runnable {
        private Socket socket;

        public Handler(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            String uuid = null;
            BufferedReader in = null;
            try {
                in = new BufferedReader(new InputStreamReader(
                        socket.getInputStream()));
                ObjectMapper mapper = new ObjectMapper();
                String line = in.readLine();
                Message message = mapper.readValue(line, Message.class);
                synchronized (users) {
                    users.put(message.getUuid(), new PrintWriter(socket.getOutputStream(), true));
                }
                uuid = message.getUuid();
                while (true) {
                    for (PrintWriter client : users.values()) {
                        client.println(line);
                    }
                    line = in.readLine();
                }

            } catch (IOException ex) {
                synchronized (System.out) {
                    System.out.println(ex.toString());
                    ex.printStackTrace();
                }
            } finally {
                if (uuid != null) {
                    synchronized (users) {
                        PrintWriter printer = users.get(uuid);
                        printer.close();
                        users.remove(uuid);
                    }
                }
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
