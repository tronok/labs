package kalashnikov.dmitry.lab4;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.UUID;

/**
 * Created by kalasd on 5/29/2014.
 */
public class Client {

    public static void main(String[] args) throws IOException {
        if (args.length != 2) {
            System.err.println("Usage: client ip port");
            return;
        }
        String ip = args[0];
        int port = Integer.parseInt(args[1]);
        String uuid = UUID.randomUUID().toString();
        try (Socket socket = new Socket(ip, port)) {
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    socket.getInputStream()));
            Thread thread = new Thread(new Printer(in));
            thread.start();
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter your name: ");
            String name = input.readLine();
            String line = "";
            ObjectMapper objectMapper = new ObjectMapper();
            while (!line.equals("exit")) {
                line = input.readLine();
                String message = objectMapper.writeValueAsString(new Message(line, name, uuid));
                out.println(message);
            }
        } catch (IOException ex) {
            System.err.println("Unable to read line");
            ex.printStackTrace();
        }


    }

    private static class Printer implements Runnable {
        private BufferedReader bufferedReader;

        public Printer(BufferedReader bufferedReader) {
            this.bufferedReader = bufferedReader;
        }

        @Override
        public void run() {
            try {

                String str;
                ObjectMapper objectMapper = new ObjectMapper();
                while ((str = bufferedReader.readLine()) != null) {
                    Message message = objectMapper.readValue(str, Message.class);
                    synchronized (System.out) {
                        System.out.println(message.toString());
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
