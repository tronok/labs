package kalashnikov.dmitry.lab2;

import kalashnikov.dmitry.lab1.ListQueue;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by Tronok on 10.04.14.
 */
public class CommandExecutor {

    public static Object execute(ListQueue<?> queue, Class<?> genericType, String methodName, Object[] params)
    throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        if (queue == null) {
            throw new NullPointerException("Queue must be not null");
        }
        if (genericType == null) {
            throw new NullPointerException("Generic type must be not null");
        }
        if (methodName == null) {
            throw new NullPointerException("Name of method must be not null");
        }
        Class[] paramsType = new Class[0];
        if (params != null) {
            paramsType = new Class[params.length];
            int ind = 0;
            for (Object param : params) {
                if (param == null) {
                    paramsType[ind] = Object.class;
                } else if (param.getClass().isAssignableFrom(genericType)) {
                    paramsType[ind] = genericType;
                } else {
                    throw new IllegalArgumentException("All params must have type assignable from type of queue's elements");
                }
            }
        } else {
            params = new Object[0];
        }

        Method[] methods = ListQueue.class.getMethods();
        Method requiredMethod = null;
        for (Method method : methods) {
            if (method.getName().equals(methodName)) {
                Class[] types = method.getParameterTypes();
                if (types.length != params.length) {
                    throw new NoSuchMethodException("Method " + methodName + " with parameters " +
                            params.toString() + " has not been found");
                }
                for (int i = 0; i != types.length; ++i) {
                    if (!types[i].isAssignableFrom(paramsType[i])) {
                        throw new NoSuchMethodException("Method " + methodName + " with parameters " +
                                params.toString() + " has not been found");
                    }
                }
                requiredMethod = method;
                break;
            }
        }
        if (requiredMethod == null) {
            throw new NoSuchMethodException("Method " + methodName + " with parameters " +
                    params.toString() + " has not been found");
        }
        return requiredMethod.invoke(queue, params);
    }

}
