package kalashnikov.dmitry.lab2;

import kalashnikov.dmitry.lab1.ListQueue;
import sun.plugin.dom.exception.InvalidStateException;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.util.StringTokenizer;

/**
 * Created by Tronok on 10.04.14.
 */
public class Main {
    public static void main(String[] args) throws IOException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        if (args.length != 2) {
            throw new InvalidStateException("Only path to input and output file must be provided");
        }
        ListQueue<Integer> queue = new ListQueue<>();
        Object result = null;
        String name = null;
        try (BufferedReader br = new BufferedReader(new FileReader(args[0]))) {
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(args[1]))) {
                StringTokenizer st = new StringTokenizer(br.readLine());
                Object[] elements = new Object[st.countTokens() - 1];
                int ind = 0;
                if (st.hasMoreElements()) {
                    name = st.nextToken();
                }
                while (st.hasMoreElements()) {
                    elements[ind] = Integer.parseInt(st.nextElement().toString());
                    ++ind;
                }
                result = CommandExecutor.execute(queue, Integer.class, name, elements);
                if (result != null) {

                    bw.write(name + " result " + result.toString() + "\n");

                }
            }
        }


    }

}
