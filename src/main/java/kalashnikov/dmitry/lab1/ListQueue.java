package kalashnikov.dmitry.lab1;

import java.util.*;

/**
 * @author Kalashnikov Dmitry
 * Generic queue based on one-way linked list which implements <code>Queue</code> iterface and
 * extends <code>AbstractCollection</code> class. All elements (including null) are permited.
 * <p>
 *     All remove operations of signle element (inluce iterator <code>remove</code>) require O(1).
 * </p>
 * @see java.util.Queue
 * @see java.util.AbstractCollection
 */
public class ListQueue<E> extends AbstractCollection<E> implements Queue<E> {

    private class Node {
        private Node next;
        private E value;

        Node() {
            this.next = null;
            this.value = null;
        }

        Node(Node next, E value) {
            this.next = next;
            this.value = value;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        public E getValue() {
            return value;
        }

        public void setValue(E value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "{" +
                    "value=" + value +
                    '}';
        }
    }

    private Node head;
    private Node tail = new Node();
    private int size = 0;

    private int decreaseSize() {
        if (size - 1 < 0) {
            size = 0;
        } else {
            --size;
        }
        return size;
    }

    private int increaseSize() {
        if (size == Integer.MAX_VALUE) {
            size = Integer.MAX_VALUE;
        } else {
            ++size;
        }
        return size;
    }

    @Override
    public E remove() {
        if (head == null) {
            throw new NoSuchElementException("Queue is empty");
        }
        return poll();
    }

    /**
     * Retrieves and removes the head of this queue, or returns null if this queue is empty.
     * Pay attention that null doesn't always mean emptiness of queue due to null elements permission. Computing time is O(1).
     *
     * @return the head of this queue, or null if this queue is empty
     */
    @Override
    public E poll() {
        if (head == null) {
            return null;
        }
        Node copy = head;
        head = head.getNext();
        decreaseSize();
        return copy.getValue();
    }

    /**
     * Retrieves, but does not remove, the head of this queue, or returns null if this queue is empty.
     * Pay attention that null doesn't always mean emptiness of queue due to null elements permission. Computing time is O(1).
     * @return the head of this queue, or null if this queue is empty.
     */
    @Override
    public E peek() {
        return head == null ? null : head.getValue();
    }

    @Override
    public E element() {
        if (head == null) {
            throw new NoSuchElementException("Queue is empty");
        }
        return peek();
    }

    /**
     * Inserts the specified element into this queue immediately without violating capacity restrictions since underlying structure is one-way list.
     *
     * @param e the element to add
     * @return always return true
     */
    @Override
    public boolean offer(E e) {
        return add(e);
    }

    /**
     * Inserts the specified element into this queue immediately without violating capacity restrictions since underlying structure is one-way list.
     * Also entails that <code>IllegalStateException </code> is never thrown. Computing time is O(1).
     *
     * @param e the element to add
     * @return always return true
     */
    @Override
    public boolean add(E e) {

        Node newElem = new Node(null, e);
        if (head == null) {
            head = newElem;
            tail = newElem;
        } else {

            tail.setNext(newElem);

            tail = newElem;
            if (head.getNext() == null) {
                head.setNext(tail);
            }
        }
        increaseSize();
        return true;
    }


    /**
     * Returns the number of elements in this list. Computing time is O(1).
     * @return the number of elements in this list
     */
    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean contains(Object o) {
        if (head == null) {
            return false;
        }
        Iterator<E> iterator = iterator();
        while (iterator.hasNext()) {
            E val = iterator.next();
            if (val == null && o == null) {
                return true;
            }
            if (val != null && val.equals(o)) {
                return true;
            }
        }
        return false;
    }

    private class QueueIterator implements Iterator<E> {
        private Node lastNode = null;
        private Node currentItem = null;
        private boolean isNext = false;
        private boolean headChanged = false;

        @Override
        public boolean hasNext() {
            return head != null && (currentItem == null ||
                    currentItem.getNext() != null);
        }

        @Override
        public E next() {
            isNext = true;
            if (head == null) {
                throw new NoSuchElementException("Queue is empty");
            }
            if (headChanged && lastNode == null) {
                currentItem = head;
                headChanged = false;
            } else if (lastNode != null && currentItem == null) {
                currentItem = lastNode.getNext();
            } else if (currentItem == null) {

                currentItem = head;
                return head.getValue();
            } else {
                lastNode = currentItem;
                currentItem = currentItem.getNext();
            }

            return currentItem.getValue();
        }

        @Override
        public void remove() {
            if (head == null || !isNext) {
                throw new IllegalStateException("Queue is empty");
            }
            decreaseSize();
            if (lastNode == null) {
                headChanged = true;
                head = head.getNext();

            } else {
                lastNode.setNext(currentItem.getNext());

            }
            isNext = false;
        }
    }

    @Override
    public Iterator<E> iterator() {
        return new QueueIterator();
    }




    @Override
    public boolean remove(Object o) {
        if (head == null) {
            return false;
        }
        Iterator<E> iterator = iterator();
        while (iterator.hasNext()) {
            E val = iterator.next();
            if (val == null && o == null || val.equals(o)) {
                decreaseSize();
                iterator.remove();
                return true;
            }

        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {

        Iterator iterator = c.iterator();
        while (iterator.hasNext()) {

            if (!contains(iterator.next())) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        Iterator iterator = c.iterator();
        while (iterator.hasNext()) {
            add((E) iterator.next());
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        Iterator iterator = c.iterator();
        while (iterator.hasNext()) {
            remove((E) iterator.next());
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {

        Iterator<E> queueIter = iterator();
        while (queueIter.hasNext()) {
            E val = queueIter.next();
            if (!c.contains(val)) {
                queueIter.remove();
            }
        }
        return true;
    }

    /**
     * Removes all of the elements from this collection. The collection will be empty after this method returns.
     * Computing time is O(1).
     */
    @Override
    public void clear() {
        head = null;
        tail = null;
        size = 0;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ListQueue)) return false;

        ListQueue listQueue = (ListQueue) o;

        if (size() != listQueue.size()) return false;
        Iterator<E> lhIter = iterator();
        Iterator rhIter = listQueue.iterator();
        while (lhIter.hasNext()) {
            E lhNext = lhIter.next();
            Object rhNext = rhIter.next();
            if (lhNext == null && rhNext != null) {
                return false;
            }
            if (lhNext != null && !lhNext.equals(rhNext)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hashCode = 1;
        for (E e : this)
            hashCode = 31 * hashCode + (e == null ? 0 : e.hashCode());
        return hashCode;
    }


}
