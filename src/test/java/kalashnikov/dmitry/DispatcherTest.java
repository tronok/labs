package kalashnikov.dmitry;

import kalashnikov.dmitry.lab3.Dispatcher;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

/**
 * Created by Tronok on 29.04.14.
 */
public class DispatcherTest {
    @Test
    public void testDispatcherNormal2Threads() throws InterruptedException, ExecutionException, IOException {
        Dispatcher dispatcher = new Dispatcher("multicoreTest.txt", 2);
        Assert.assertArrayEquals(new int[]{1, 2, 2, 3, 5, 5, 6, 8, 9, 34}, dispatcher.sort());
        dispatcher = new Dispatcher("multicoreTest_1_by_1.txt", 2);
        Assert.assertArrayEquals(new int[]{1, 2, 2, 3, 5, 5, 6, 8, 9, 34}, dispatcher.sort());
    }

    @Test
    public void testDispatcherNormal3Threads() throws InterruptedException, ExecutionException, IOException {
        Dispatcher dispatcher = new Dispatcher("multicoreTest.txt", 3);
        Assert.assertArrayEquals(new int[]{1, 2, 2, 3, 5, 5, 6, 8, 9, 34}, dispatcher.sort());
        dispatcher = new Dispatcher("multicoreTest_1_by_1.txt", 3);
        Assert.assertArrayEquals(new int[]{1, 2, 2, 3, 5, 5, 6, 8, 9, 34}, dispatcher.sort());
    }

    @Test
    public void testDispatcherNormal4Threads() throws InterruptedException, ExecutionException, IOException {
        Dispatcher dispatcher = new Dispatcher("multicoreTest.txt", 4);
        Assert.assertArrayEquals(new int[]{1, 2, 2, 3, 5, 5, 6, 8, 9, 34}, dispatcher.sort());
        dispatcher = new Dispatcher("multicoreTest_1_by_1.txt", 4);
        Assert.assertArrayEquals(new int[]{1, 2, 2, 3, 5, 5, 6, 8, 9, 34}, dispatcher.sort());
    }
}

