package kalashnikov.dmitry;

import kalashnikov.dmitry.lab3.SortTask;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Tronok on 29.04.14.
 */

public class SortTaskTest {
    @Test
    public void testSortTaskNormal() throws Exception {
        String[] str = new String[4];
        str[0] = "4";
        str[1] = "1";
        str[2] = "3";
        str[3] = "2";
        SortTask st = new SortTask(str);
        int[] res = st.call();
        Assert.assertArrayEquals(new int[]{1, 2, 3, 4}, res);
    }

    @Test(expected = NumberFormatException.class)
    public void testSortTaskWithNull() throws Exception {
        String[] str = new String[4];
        str[0] = "4";
        str[1] = null;
        str[2] = "3";
        str[3] = "2";
        SortTask st = new SortTask(str);
        int[] res = st.call();
        Assert.assertArrayEquals(new int[]{1, 2, 3, 4}, res);
    }

    @Test
    public void testSortTaskEmpty() throws Exception {
        String[] str = new String[0];
        SortTask st = new SortTask(str);
        int[] res = st.call();
        Assert.assertArrayEquals(new int[]{}, res);
    }
}
