package kalashnikov.dmitry;

import kalashnikov.dmitry.lab1.ListQueue;
import kalashnikov.dmitry.lab2.CommandExecutor;
import kalashnikov.dmitry.lab2.Main;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import static org.junit.Assert.assertTrue;


/**
 * Created by Tronok on 10.04.14.
 */
public class CommandExecutorTest {

    @Test(expected = NullPointerException.class)
    public void testExecutionNullQueue() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        CommandExecutor.execute(null, Integer.class, "", new Object[]{});
    }

    @Test(expected = NullPointerException.class)
    public void testExecutionNullGenericType() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        CommandExecutor.execute(new ListQueue<Object>(), null, "", new Object[]{});
    }

    @Test(expected = NullPointerException.class)
    public void testExecutionNullMethodName() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        CommandExecutor.execute(new ListQueue<Object>(), Integer.class, null, new Object[]{});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExecutionWithIncorrectTypes() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        CommandExecutor.execute(new ListQueue<Integer>(), Integer.class, "add", new Object[]{1, null, "2"});
    }

    @Test
    public void testMethodWithOneParamExecution() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        CommandExecutor.execute(new ListQueue<Integer>(), Integer.class, "add", new Object[]{1});
    }

    @Test(expected = NoSuchMethodException.class)
    public void testMethodWithOneParamExecutionFailTooManyArgs() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        CommandExecutor.execute(new ListQueue<Integer>(), Integer.class, "add", new Object[]{1, 2});

    }

    @Test(expected = IllegalArgumentException.class)
    public void testMethodWithOneParamExecutionFailIncorrectType() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        CommandExecutor.execute(new ListQueue<Integer>(), Integer.class, "add", new Object[]{"2"});

    }

    @Test
    public void testMethodWithoutParamExecution() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        CommandExecutor.execute(new ListQueue<Integer>(), Integer.class, "clear", null);
        CommandExecutor.execute(new ListQueue<Integer>(), Integer.class, "clear", new Object[]{});
    }

    @Test(expected = NullPointerException.class)
    public void testMethodExecutionFailIncorrectName() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        CommandExecutor.execute(new ListQueue<Integer>(), Integer.class, "cleart", null);
    }

    @Test
    public void testExecutionFromFile() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, IOException {
        Main.main(new String[]{"testData.txt", "result.txt"});
        try (BufferedReader br = new BufferedReader(new FileReader("result.txt"))) {
            assertTrue(br.readLine().equals("add result true"));
        }

    }

    @Test(expected = NumberFormatException.class)
    public void testExecutionFromFileFailDueType() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, IOException {
        Main.main(new String[]{"testDataFail.txt", "result.txt"});
    }
}
