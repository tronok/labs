package kalashnikov.dmitry;

import kalashnikov.dmitry.lab1.ListQueue;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * Created by Tronok on 08.03.14.
 */
public class ListQueueTest {


    @Test(expected = IllegalStateException.class)
    public void testDoubleRemove() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(-2);
        Iterator iter = testList.iterator();
        iter.next();
        iter.remove();
        iter.remove();
    }

    @Test
    public void testRemoveNextRemoveNext() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(-2);
        Iterator iter = testList.iterator();
        iter.next();
        iter.remove();
        iter.next();
        iter.remove();
        assertArrayEquals(new Object[]{-2}, testList.toArray());
    }

    @Test
    public void testAddStringWithPeek() {
        ListQueue<String> testList = new ListQueue<>();
        testList.add("test");
        assertThat(testList.peek(), is("test"));
    }

    @Test
    public void testAddNullWithPeek() {
        ListQueue<String> testList = new ListQueue<>();
        testList.add(null);
        assertNull(testList.peek());
    }

    @Test
    public void testAddIntegerWithPeek() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(4);
        assertThat(testList.peek(), is(4));
    }

    @Test(expected = NoSuchElementException.class)
    public void testElementEmptyList() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.element();
    }

    @Test
    public void testRemoveHead() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(4);
        assertThat(testList.remove(), is(4));
        assertNull(testList.peek());
    }

    @Test(expected = NoSuchElementException.class)
    public void testRemoveHeadFromEmpty() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.remove();
    }

    @Test
    public void testSizeAfterAdd() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        assertThat(testList.size(), is(2));
    }

    @Test
    public void testSizeAfterAddAndDelete() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.remove();
        assertThat(testList.size(), is(1));
    }

    @Test
    public void testContainItemAfterAdd() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(-2);
        assertTrue(testList.contains(5));
    }

    @Test
    public void testContainItemAfterAddAndDelete() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(-2);
        testList.remove(5);
        assertFalse(testList.contains(5));
    }

    @Test
    public void testContainNullAfterAdd() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(null);
        assertTrue(testList.contains(null));
    }

    @Test
    public void testContainNullAfterAddAndDelete() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(null);
        testList.remove(2);
        assertTrue(testList.contains(null));
    }

    @Test
    public void testHeadAfterAddAndDelete() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(4);
        assertTrue(testList.remove(2));
        assertThat(testList.peek(), is(5));
    }

    @Test
    public void testHeadAfterFalseDelete() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(4);
        assertFalse(testList.remove(100));
        assertThat(testList.peek(), is(2));
    }

    @Test
    public void testRemoveNull() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        assertFalse(testList.remove(null));
        testList.add(null);
        assertTrue(testList.remove(null));
    }

    @Test
    public void testContainsAllWithoutNull() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(4);
        ArrayList<Integer> contains = new ArrayList<>();
        contains.add(2);
        contains.add(4);
        assertTrue(testList.containsAll(contains));
    }

    @Test
    public void testContainsAllWithNull() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(4);
        testList.add(null);
        ArrayList<Integer> contains = new ArrayList<>();
        contains.add(null);
        contains.add(2);
        contains.add(4);
        assertTrue(testList.containsAll(contains));
    }

    @Test
    public void testContainsAllIncorrectType() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(4);
        ArrayList<String> contains = new ArrayList<>();
        contains.add(null);
        contains.add("1");
        contains.add("2");
        assertFalse(testList.containsAll(contains));
    }

    @Test
    public void testAddAllWithNull() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(4);
        ArrayList<Integer> toAdd = new ArrayList<>();
        toAdd.add(null);
        toAdd.add(2);
        toAdd.add(10);
        testList.addAll(toAdd);
        assertTrue(testList.containsAll(toAdd));
    }

    @Test
    public void testRemoveAllWithNull() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(4);
        ArrayList<Integer> toDelete = new ArrayList<>();
        toDelete.add(null);
        toDelete.add(2);
        toDelete.add(4);
        testList.removeAll(toDelete);
        assertThat(testList.peek(), is(5));
    }

    @Test
    public void testRetainAll() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(4);
        ArrayList<Integer> retain = new ArrayList<>();
        retain.add(null);
        retain.add(2);
        retain.add(4);
        testList.retainAll(retain);
        assertFalse(testList.contains(5));
    }

    @Test
    public void testRetainAllOneElement() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(4);
        ArrayList<Integer> retain = new ArrayList<>();
        retain.add(5);
        testList.retainAll(retain);
        assertTrue(testList.contains(5));
    }

    @Test
    public void testToArrayWithNull() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(4);
        testList.add(null);
        assertArrayEquals(new Object[]{2, 5, 4, null}, testList.toArray());
    }

    @Test
    public void testIteratorNext() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(4);
        testList.add(null);
        Iterator<Integer> iter = testList.iterator();
        ArrayList<Integer> copy = new ArrayList<>();
        while (iter.hasNext()) {
            copy.add(iter.next());
        }
        assertArrayEquals(new Object[]{2, 5, 4, null}, copy.toArray());

    }

    @Test
    public void testIteratorNextOvercomeRange() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(4);
        testList.add(null);
        Iterator<Integer> iter = testList.iterator();
        ArrayList<Integer> copy = new ArrayList<>();
        while (iter.hasNext()) {
            copy.add(iter.next());
        }
        assertArrayEquals(new Object[]{2, 5, 4, null}, copy.toArray());

    }

    @Test(expected = NoSuchElementException.class)
    public void testIteratorNextEmptyList() {
        ListQueue<Integer> testList = new ListQueue<>();
        Iterator<Integer> iter = testList.iterator();
        assertFalse(iter.hasNext());
        iter.next();
    }

    @Test(expected = IllegalStateException.class)
    public void testIteratorRemoveEmptyList() {
        ListQueue<Integer> testList = new ListQueue<>();
        Iterator<Integer> iter = testList.iterator();
        assertFalse(iter.hasNext());
        iter.remove();
    }

    @Test
    public void testIteratorRemove() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(4);
        testList.add(null);
        Iterator<Integer> iter = testList.iterator();
        iter.next();
        iter.next();
        iter.remove();
        assertTrue(iter.hasNext());
        assertArrayEquals(new Object[]{2, 4, null}, testList.toArray());
    }

    @Test(expected = NullPointerException.class)
    public void testToArrayGenericNullArray() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.toArray(null);
    }

    @Test(expected = ArrayStoreException.class)
    public void testToArrayGenericIncorrectTypeOfPassedArray() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(4);
        testList.toArray(new String[]{"1", "2"});
    }

    @Test
    public void testToArrayGenericArraySmaller() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(4);
        assertArrayEquals(new Object[]{2, 5, 4}, testList.toArray(new Integer[]{1}));

    }

    @Test
    public void testToArrayGenericArrayEqual() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(4);
        assertArrayEquals(new Object[]{2, 5, 4}, testList.toArray(new Integer[3]));
    }

    @Test
    public void testToArrayGenericArrayLarger() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(4);
        assertArrayEquals(new Object[]{2, 5, 4, null, 5},
                testList.toArray(new Integer[]{1, 2, 3, 4, 5}));
    }

    @Test
    public void testSizeAfterPoll() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.poll();
        assertThat(testList.size(), is(1));
    }

    @Test
    public void testEqualsIsEqual() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(null);
        testList.add(-1);
        ListQueue<Integer> testList2 = new ListQueue<>();
        testList2.add(2);
        testList2.add(5);
        testList2.add(null);
        testList2.add(-1);
        assertTrue(testList.equals(testList2));
    }

    @Test
    public void testEqualsIsNotEqualNull() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(10);
        testList.add(-1);
        ListQueue<Integer> testList2 = new ListQueue<>();
        testList2.add(2);
        testList2.add(5);
        testList2.add(null);
        testList2.add(-1);
        assertFalse(testList.equals(testList2));
    }

    @Test
    public void testEqualsIsNotEqualNormal() {
        ListQueue<Integer> testList = new ListQueue<>();
        testList.add(2);
        testList.add(5);
        testList.add(null);
        testList.add(25);
        ListQueue<Integer> testList2 = new ListQueue<>();
        testList2.add(2);
        testList2.add(5);
        testList2.add(null);
        testList2.add(-1);
        assertFalse(testList.equals(testList2));
    }

}
