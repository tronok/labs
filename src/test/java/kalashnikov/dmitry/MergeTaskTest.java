package kalashnikov.dmitry;

import kalashnikov.dmitry.lab3.MergeTask;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Tronok on 29.04.14.
 */
public class MergeTaskTest {
    @Test
    public void testNormalMerge() throws Exception {
        int[] first = new int[]{1, 5, 20, 40};
        int[] second = new int[]{0, 9, 20};
        MergeTask mt = new MergeTask(first, second);
        Assert.assertArrayEquals(new int[]{0, 1, 5, 9, 20, 20, 40}, mt.call());
    }

    @Test(expected = NullPointerException.class)
    public void testMergeWithNull() {
        int[] second = new int[]{0, 9, 20};
        MergeTask mt = new MergeTask(null, second);
    }

    @Test
    public void testMergeWithEmpty() throws Exception {
        int[] first = new int[]{};
        int[] second = new int[]{0, 9, 20};
        MergeTask mt = new MergeTask(first, second);
        Assert.assertArrayEquals(new int[]{0, 9, 20}, mt.call());
        mt = new MergeTask(second, first);
        Assert.assertArrayEquals(new int[]{0, 9, 20}, mt.call());
    }
}
